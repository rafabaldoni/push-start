function start(levelnumber) {
    
    if (levelnumber == 0) {
        
        level_name0 = new PIXI.Sprite(PIXI.loader.resources["images/growing.png"].texture);
        level_name0.anchor.set(0.5 , 0.5);
        level_name0.scale.set(0.5 , 0.5);
        level_name0.position.set(app.renderer.width/2 , app.renderer.height*1/8);
        app.stage.addChild(level_name0);
        
        object_start0 = new PIXI.Sprite(PIXI.loader.resources["images/square_red.png"].texture);
        object_start0.anchor.set(0.5 , 0.5);
        object_start0.scale.set(0.5 , 0.5);
        object_start0.position.set(app.renderer.width/4 , app.renderer.height/2);
        t.makeInteractive(object_start0);
        app.stage.addChild(object_start0);
        
    }
    
    if (levelnumber == 1) {
        
        level_name1 = new PIXI.Sprite(PIXI.loader.resources["images/let_it_blue.png"].texture);
        level_name1.anchor.set(0.5 , 0.5);
        level_name1.scale.set(0.5 , 0.5);
        level_name1.position.set(app.renderer.width/2 , app.renderer.height*1/8);
        app.stage.addChild(level_name1);
        
        object_start1 = new PIXI.Sprite(PIXI.loader.resources["images/square_red.png"].texture);
        object_start1.anchor.set(0.5 , 0.5);
        object_start1.scale.set(0.5 , 0.5);
        object_start1.position.set(app.renderer.width/4 , app.renderer.height/2);
        t.makeInteractive(object_start1);
        app.stage.addChild(object_start1);
        
    }
    
    if (levelnumber == 2) {
        
        level_name2 = new PIXI.Sprite(PIXI.loader.resources["images/pulsating.png"].texture);
        level_name2.anchor.set(0.5 , 0.5);
        level_name2.scale.set(0.5 , 0.5);
        level_name2.position.set(app.renderer.width/2 , app.renderer.height*1/8);
        app.stage.addChild(level_name2);
        
        object_start2 = new PIXI.Sprite(PIXI.loader.resources["images/rectangle_red.png"].texture);
        object_start2.anchor.set(0.5 , 0.5);
        object_start2.scale.set(0.5 , 0.5);
        object_start2.position.set(app.renderer.width/4 , app.renderer.height/2);
        t.makeInteractive(object_start2);
        app.stage.addChild(object_start2);
        
    }
    
    if (levelnumber == 3) {
        
        level_name3 = new PIXI.Sprite(PIXI.loader.resources["images/mix_and_match.png"].texture);
        level_name3.anchor.set(0.5 , 0.5);
        level_name3.scale.set(0.5 , 0.5);
        level_name3.position.set(app.renderer.width/2 , app.renderer.height*1/8);
        app.stage.addChild(level_name3);
        
        object_start3 = new PIXI.Sprite(PIXI.loader.resources["images/rectangle_red.png"].texture);
        object_start3.anchor.set(0.5 , 0.5);
        object_start3.scale.set(0.5 , 0.5);
        object_start3.position.set(app.renderer.width/4 , app.renderer.height/2);
        t.makeInteractive(object_start3);
        app.stage.addChild(object_start3);
        
    }
    
    if (levelnumber == 4) {
        
        level_name4 = new PIXI.Sprite(PIXI.loader.resources["images/time_to_choose.png"].texture);
        level_name4.anchor.set(0.5 , 0.5);
        level_name4.scale.set(0.5 , 0.5);
        level_name4.position.set(app.renderer.width/2 , app.renderer.height*1/8);
        app.stage.addChild(level_name4);
        
        level_choose = new PIXI.Sprite(PIXI.loader.resources["images/choose.png"].texture);
        level_choose.anchor.set(0.5 , 0.5);
        level_choose.scale.set(0.3 , 0.3);
        level_choose.position.set(app.renderer.width/2 , app.renderer.height*6/8);
        app.stage.addChild(level_choose);
        
        object_start4 = new PIXI.Sprite(PIXI.loader.resources["images/square_red.png"].texture);
        object_start4.anchor.set(0.5 , 0.5);
        object_start4.scale.set(0.5 , 0.5);
        object_start4.position.set(app.renderer.width/4 , app.renderer.height/2);
        t.makeInteractive(object_start4);
        app.stage.addChild(object_start4);
        
    }
    
}

function objective(levelnumber) {
    
    line = new PIXI.Sprite(PIXI.loader.resources["images/line.png"].texture);
    line.anchor.set(0.5 , 0.5);
    line.scale.set(0.5 , 0.5);
    line.position.set(app.renderer.width/2 , app.renderer.height/2);
    app.stage.addChild(line);
    
    if (levels[levelnumber].final.size == 2) {
        objective_rectangle_red = new PIXI.Sprite(PIXI.loader.resources["images/objective_rectangle_red.png"].texture);
        objective_rectangle_red.anchor.set(0.5 , 0.5);
        objective_rectangle_red.scale.set(0.5 , 0.5);
        objective_rectangle_red.position.set(app.renderer.width*3/4 , app.renderer.height/2);
        app.stage.addChild(objective_rectangle_red);
    }
    
    if (levels[levelnumber].final.size == 1) {
        objective_square_blue = new PIXI.Sprite(PIXI.loader.resources["images/objective_square_blue.png"].texture);
        objective_square_blue.anchor.set(0.5 , 0.5);
        objective_square_blue.scale.set(0.5 , 0.5);
        objective_square_blue.position.set(app.renderer.width*3/4 , app.renderer.height/2);
        app.stage.addChild(objective_square_blue);
    }
    
}

function transformations_objects(levelnumber) {
    
    if (levelnumber == 0) {
        
        rectangle_red0 = new PIXI.Sprite(PIXI.loader.resources["images/rectangle_red.png"].texture);
        rectangle_red0.anchor.set(0.5 , 0.5);
        rectangle_red0.scale.set(0.5 , 0.2);
        rectangle_red0.position.set(app.renderer.width/2 , app.renderer.height/2);
        app.stage.addChild(rectangle_red0);
        
        square_increase0 = new PIXI.Sprite(PIXI.loader.resources["images/square_increase.png"].texture);
        square_increase0.anchor.set(0.5 , 0.5);
        square_increase0.scale.set(0.5 , 0.5);
        square_increase0.position.set(app.renderer.width/2 , app.renderer.height/2);
        app.stage.addChild(square_increase0);
    }
    
    if (levelnumber == 1) {
        square_blue1 = new PIXI.Sprite(PIXI.loader.resources["images/square_blue.png"].texture);
        square_blue1.anchor.set(0.5 , 0.5);
        square_blue1.scale.set(0.5 , 0.2);
        square_blue1.position.set(app.renderer.width/2 , app.renderer.height/2);
        app.stage.addChild(square_blue1);
        
        square_color1 = new PIXI.Sprite(PIXI.loader.resources["images/square_color.png"].texture);
        square_color1.anchor.set(0.5 , 0.5);
        square_color1.scale.set(0.5 , 0.5);
        square_color1.position.set(app.renderer.width/2 , app.renderer.height/2);
        app.stage.addChild(square_color1);
    }
    
    if (levelnumber == 2) {
        square_red2 = new PIXI.Sprite(PIXI.loader.resources["images/square_red.png"].texture);
        square_red2.anchor.set(0.5 , 0.5);
        square_red2.scale.set(0.5 , 0.5);
        square_red2.position.set(app.renderer.width*3/8 , app.renderer.height/2);
        app.stage.addChild(square_red2);
        
        rectangle_red_transformation2 = new PIXI.Sprite(PIXI.loader.resources["images/rectangle_red.png"].texture);
        rectangle_red_transformation2.anchor.set(0.5 , 0.5);
        rectangle_red_transformation2.scale.set(0.5 , 0.5);
        rectangle_red_transformation2.position.set(app.renderer.width*3/8 , app.renderer.height/2);
        app.stage.addChild(rectangle_red_transformation2);
        rectangle_red_transformation2.visible = false;
        
        rectangle_red2 = new PIXI.Sprite(PIXI.loader.resources["images/rectangle_red.png"].texture);
        rectangle_red2.anchor.set(0.5 , 0.5);
        rectangle_red2.scale.set(0.5 , 0.2);
        rectangle_red2.position.set(app.renderer.width*5/8 , app.renderer.height/2);
        app.stage.addChild(rectangle_red2);
        
        square_decrease2 = new PIXI.Sprite(PIXI.loader.resources["images/square_decrease.png"].texture);
        square_decrease2.anchor.set(0.5 , 0.5);
        square_decrease2.scale.set(0.5 , 0.5);
        square_decrease2.position.set(app.renderer.width*3/8 , app.renderer.height/2);
        app.stage.addChild(square_decrease2);
        
        square_increase2 = new PIXI.Sprite(PIXI.loader.resources["images/square_increase.png"].texture);
        square_increase2.anchor.set(0.5 , 0.5);
        square_increase2.scale.set(0.5 , 0.5);
        square_increase2.position.set(app.renderer.width*5/8 , app.renderer.height/2);
        app.stage.addChild(square_increase2);
    }
    
    if (levelnumber == 3) {
        rectangle_blue3 = new PIXI.Sprite(PIXI.loader.resources["images/rectangle_blue.png"].texture);
        rectangle_blue3.anchor.set(0.5 , 0.5);
        rectangle_blue3.scale.set(0.5 , 0.2);
        rectangle_blue3.position.set(app.renderer.width*3/8 , app.renderer.height/2);
        app.stage.addChild(rectangle_blue3);
        rectangle_blue3.visible = false;
        
        rectangle_blue_transformation3 = new PIXI.Sprite(PIXI.loader.resources["images/rectangle_blue.png"].texture);
        rectangle_blue_transformation3.anchor.set(0.5 , 0.5);
        rectangle_blue_transformation3.scale.set(0.5 , 0.5);
        rectangle_blue_transformation3.position.set(app.renderer.width*5/8 , app.renderer.height/2);
        app.stage.addChild(rectangle_blue_transformation3);
        rectangle_blue_transformation3.visible = false;
        
        square_blue3 = new PIXI.Sprite(PIXI.loader.resources["images/square_blue.png"].texture);
        square_blue3.anchor.set(0.5 , 0.5);
        square_blue3.scale.set(0.5 , 0.5);
        square_blue3.position.set(app.renderer.width*5/8 , app.renderer.height/2);
        app.stage.addChild(square_blue3);
        
        square_color3 = new PIXI.Sprite(PIXI.loader.resources["images/square_color.png"].texture);
        square_color3.anchor.set(0.5 , 0.5);
        square_color3.scale.set(0.5 , 0.5);
        square_color3.position.set(app.renderer.width*3/8 , app.renderer.height/2);
        app.stage.addChild(square_color3);
        
        square_decrease3 = new PIXI.Sprite(PIXI.loader.resources["images/square_decrease.png"].texture);
        square_decrease3.anchor.set(0.5 , 0.5);
        square_decrease3.scale.set(0.5 , 0.5);
        square_decrease3.position.set(app.renderer.width*5/8 , app.renderer.height/2);
        app.stage.addChild(square_decrease3);
    }
    
    if (levelnumber == 4) {
        square_blue4 = new PIXI.Sprite(PIXI.loader.resources["images/square_blue.png"].texture);
        square_blue4.anchor.set(0.5 , 0.5);
        square_blue4.scale.set(0.5 , 0.5);
        square_blue4.position.set(app.renderer.width/2 , app.renderer.height/2);
        app.stage.addChild(square_blue4);
        
        square_red4 = new PIXI.Sprite(PIXI.loader.resources["images/square_red.png"].texture);
        square_red4.anchor.set(0.5 , 0.5);
        square_red4.scale.set(0.5 , 0.5);
        square_red4.position.set(app.renderer.width/2 , app.renderer.height/2);
        app.stage.addChild(square_red4);
        
        rectangle_red4 = new PIXI.Sprite(PIXI.loader.resources["images/rectangle_red.png"].texture);
        rectangle_red4.anchor.set(0.5 , 0.5);
        rectangle_red4.scale.set(0.5 , 0.2);
        rectangle_red4.position.set(app.renderer.width/2 , app.renderer.height/2);
        app.stage.addChild(rectangle_red4);
        
        rectangle_red5 = new PIXI.Sprite(PIXI.loader.resources["images/rectangle_red.png"].texture);
        rectangle_red5.anchor.set(0.5 , 0.5);
        rectangle_red5.scale.set(0.5 , 0.2);
        rectangle_red5.position.set(app.renderer.width/2 , app.renderer.height/2);
        app.stage.addChild(rectangle_red5);
        
        rectangle_red6 = new PIXI.Sprite(PIXI.loader.resources["images/rectangle_red.png"].texture);
        rectangle_red6.anchor.set(0.5 , 0.5);
        rectangle_red6.scale.set(0.5 , 0.2);
        rectangle_red6.position.set(app.renderer.width/2 , app.renderer.height/2);
        app.stage.addChild(rectangle_red6);
        
        square_choose4 = new PIXI.Sprite(PIXI.loader.resources["images/square_choose.png"].texture);
        square_choose4.anchor.set(0.5 , 0.5);
        square_choose4.scale.set(0.5 , 0.5);
        square_choose4.position.set(app.renderer.width/2 , app.renderer.height/2);
        app.stage.addChild(square_choose4);
        
        square_increase4 = new PIXI.Sprite(PIXI.loader.resources["images/square_increase.png"].texture);
        square_increase4.anchor.set(0.5 , 0.5);
        square_increase4.scale.set(0.5 , 0.5);
        square_increase4.position.set(app.renderer.width*3/8 , app.renderer.height*7/8);
        t.makeInteractive(square_increase4);
        app.stage.addChild(square_increase4);
        
        square_decrease4 = new PIXI.Sprite(PIXI.loader.resources["images/square_decrease.png"].texture);
        square_decrease4.anchor.set(0.5 , 0.5);
        square_decrease4.scale.set(0.5 , 0.5);
        square_decrease4.position.set(app.renderer.width/2 , app.renderer.height*7/8);
        t.makeInteractive(square_decrease4);
        app.stage.addChild(square_decrease4);
        
        square_color4 = new PIXI.Sprite(PIXI.loader.resources["images/square_color.png"].texture);
        square_color4.anchor.set(0.5 , 0.5);
        square_color4.scale.set(0.5 , 0.5);
        square_color4.position.set(app.renderer.width*5/8 , app.renderer.height*7/8);
        t.makeInteractive(square_color4);
        app.stage.addChild(square_color4);
    }
}

function pre_level4() {
    t.update();
            
        if ((pointer.hitTestSprite(square_increase4) || pointer.hitTestSprite(square_decrease4) || pointer.hitTestSprite(square_color4) || pointer.hitTestSprite(object_start4) ) && object_start4.x == app.renderer.width/4) {
                pointer.cursor = "pointer";
            }
        else {
            pointer.cursor = "auto";
        }
    
        square_increase4.release = () => {
            square_choose4 = new PIXI.Sprite(PIXI.loader.resources["images/square_increase.png"].texture);
            square_choose4.anchor.set(0.5 , 0.5);
            square_choose4.scale.set(0.5 , 0.5);
            square_choose4.position.set(app.renderer.width/2 , app.renderer.height/2);
            app.stage.addChild(square_choose4);
            choose = "increase";
        }
        
        square_decrease4.release = () => {
            square_choose4 = new PIXI.Sprite(PIXI.loader.resources["images/square_decrease.png"].texture);
            square_choose4.anchor.set(0.5 , 0.5);
            square_choose4.scale.set(0.5 , 0.5);
            square_choose4.position.set(app.renderer.width/2 , app.renderer.height/2);
            app.stage.addChild(square_choose4);
            choose = "decrease";
        }
        
        square_color4.release = () => {
            square_choose4 = new PIXI.Sprite(PIXI.loader.resources["images/square_color.png"].texture);
            square_choose4.anchor.set(0.5 , 0.5);
            square_choose4.scale.set(0.5 , 0.5);
            square_choose4.position.set(app.renderer.width/2 , app.renderer.height/2);
            app.stage.addChild(square_choose4);
            choose = "color";
        }
}

function transformations(levelnumber) {

    if (levelnumber == 0) {
        
        TweenMax.to(object_start0.position, 2.5, {x:app.renderer.width/2});
        gameapp.ticker.add(delta => insideLoop0(delta));
        
        function insideLoop0(delta) {
            if (object_start0.x == app.renderer.width/2) {
                object_start0.x = app.renderer.width/4;
                object_start0.visible = false;
                TweenMax.to(rectangle_red0.scale, 2.5, {y:0.5});
            }
            if (rectangle_red0.scale.y == 0.5 && rectangle_red0.x == app.renderer.width/2) {
                TweenMax.to(rectangle_red0.position, 2.5, {x:app.renderer.width*3/4});
            }
            if (rectangle_red0.x == app.renderer.width*3/4) {
                end_size[0] = 2;
                end_color[0] = "#ff0000";
            }
        }
    }
    
    if (levelnumber == 1) {
        
        TweenMax.to(object_start1.position, 2.5, {x:app.renderer.width/2});
        gameapp.ticker.add(delta => insideLoop1(delta));
        
        function insideLoop1(delta) {
            if (object_start1.x == app.renderer.width/2) {
                object_start1.x = app.renderer.width/4;
                object_start1.visible = false;
                TweenMax.to(square_blue1.scale, 1.5, {y:0.5});
            }
            if (square_blue1.scale.y == 0.5 && square_blue1.x == app.renderer.width/2) {
                TweenMax.to(square_blue1.position, 2.5, {x:app.renderer.width*3/4});
            }
            if (square_blue1.x == app.renderer.width*3/4) {
                end_size[1] = 1;
                end_color[1] = "#0000ff";
            }
        }
    }
    
    if (levelnumber == 2) {
        
        TweenMax.to(object_start2.position, 1.5, {x:app.renderer.width*3/8});
        gameapp.ticker.add(delta => insideLoop2(delta));
        
        function insideLoop2(delta) {
            if (object_start2.x == app.renderer.width*3/8) {
                object_start2.x = app.renderer.width/4;
                object_start2.visible = false;
                rectangle_red_transformation2.visible = true;
                TweenMax.to(rectangle_red_transformation2.scale, 2.5, {y:0.2});
            }
            if (rectangle_red_transformation2.scale.y == 0.2) {
                rectangle_red_transformation2.scale.y = 0.5;
                rectangle_red_transformation2.visible = false;
                TweenMax.to(square_red2.position, 1.5, {x:app.renderer.width*5/8});
            }
            if (square_red2.position.x == app.renderer.width*5/8) {
                square_red2.x = app.renderer.width*3/8;
                TweenMax.to(rectangle_red2.scale, 2.5, {y:0.5});
            }
            if (rectangle_red2.scale.y == 0.5 && rectangle_red2.x == app.renderer.width*5/8) {
                TweenMax.to(rectangle_red2.position, 1.5, {x:app.renderer.width*3/4});
            }
            if (rectangle_red2.x == app.renderer.width*3/4) {
                end_size[2] = 2;
                end_color[2] = "#ff0000";
            }
        }
        
    }
    
    if (levelnumber == 3) {
        
        TweenMax.to(object_start3.position, 1.5, {x:app.renderer.width*3/8});
        gameapp.ticker.add(delta => insideLoop3(delta));
        
        function insideLoop3(delta) {
            if (object_start3.x == app.renderer.width*3/8 && rectangle_blue3.scale.y == 0.2) {
                TweenMax.to(rectangle_blue3.scale, 1.5, {y:0.5})
            }
            if (rectangle_blue3.scale.y == 0.5 && rectangle_blue3.x == app.renderer.width*3/8) {
                object_start3.x = app.renderer.width/4;
                object_start3.visible = false;
                rectangle_blue3.visible = true;
                TweenMax.to(rectangle_blue3.position, 1.5, {x:app.renderer.width*5/8});
            }
            if (rectangle_blue3.x == app.renderer.width*5/8) {
                rectangle_blue3.x = app.renderer.width*3/8;
                rectangle_blue3.scale.y = 0.2;
                rectangle_blue3.visible = false;
                rectangle_blue_transformation3.visible = true;
                TweenMax.to(rectangle_blue_transformation3.scale, 2.5, {y:0.2});
            }
            if (rectangle_blue_transformation3.scale.y == 0.2) {
                rectangle_blue_transformation3.scale.y = 0.5;
                rectangle_blue_transformation3.visible = false;
                TweenMax.to(square_blue3.position, 1.5, {x:app.renderer.width*3/4});
            }
            if (square_blue3.x == app.renderer.width*3/4) {
                end_size[3] = 1;
                end_color[3] = "#0000ff";
            }
        }
        
    }
    
    if (levelnumber == 4) {
        
        TweenMax.to(object_start4.position, 2.5, {x:app.renderer.width/2});
        gameapp.ticker.add(delta => insideLoop4(delta));
        
        function insideLoop4(delta) {
            if (choose == "increase") {
                if (object_start4.x == app.renderer.width/2) {
                    object_start4.x = app.renderer.width/4;
                    object_start4.visible = false;
                    TweenMax.to(rectangle_red4.scale, 2.5, {y:0.5});
                }
                if (rectangle_red4.scale.y == 0.5) {
                    TweenMax.to(rectangle_red4.position, 2.5, {x:app.renderer.width*3/4});
                }
                if (rectangle_red4.x == app.renderer.width*3/4) {
                    end_size[4] = 2;
                    end_color[4] = "#ff0000";
                    
                }
            }
            
            if (choose == "decrease") {
                if (object_start4.x == app.renderer.width/2) {
                    object_start4.x = app.renderer.width/4;
                    object_start4.visible = false;
                    rectangle_red5.visible = false;
                    TweenMax.to(rectangle_red5.scale, 1.5, {y:0.5})
                }
                if (rectangle_red4.x != app.renderer.width/2){
                    object_start4.destroy();
                }
                if (rectangle_red5.scale.y == 0.5) {
                    rectangle_red5.scale.y = 0.2;
                    TweenMax.to(square_red4.position, 2.5, {x:app.renderer.width*3/4});
                }
                if (square_red4.x == app.renderer.width*3/4) {
                   // end_size[4] = 1;
                   // end_color[4] = "#ff0000";
                }
            }
            
            if (choose == "color") {
                if (object_start4.x == app.renderer.width/2) {
                    object_start4.x = app.renderer.width/4;
                    object_start4.visible = false;
                    rectangle_red6.visible = false;
                    TweenMax.to(rectangle_red6.scale, 1.5, {y:0.5})
                }
                if (rectangle_red4.x != app.renderer.width/2) {
                    object_start4.destroy();
                }
                if (rectangle_red6.scale.y == 0.5) {
                    rectangle_red6.scale.y = 0.2;
                    TweenMax.to(square_blue4.position, 2.5, {x:app.renderer.width*3/4});
                }
                if (square_blue4.x == app.renderer.width*3/4) {
                   // end_size[4] = 1;
                    //end_color[4] = "#0000ff";
                }
            }
            
            if (choose == "") {
                if (rectangle_red4.x != app.renderer.width/2) {
                    object_start4.destroy();
                }
                square_choose4.visible = false;
                square_blue4.visible = false;
                square_red4.visible = false;
                rectangle_red4.visible = false;
                rectangle_red5.visible = false;
                rectangle_red6.visible = false;
                if (object_start4.x == app.renderer.width/2) {
                    TweenMax.to(object_start4.position, 2.5, {x:app.renderer.width*3/4});
                }
                if (object_start4.x == app.renderer.width*3/4) {
                   // end_size[4] = 1;
                   // end_color[4] = "#ff0000";
                }
            }
        }
        
    }
    
}